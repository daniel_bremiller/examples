package examples.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by dlbremil on 11/29/17.
 */
public class SocketLoggingClient implements Client{
    private int messageCount = 20;
    int clientId;

    public SocketLoggingClient(int id, int messageCount) {
        clientId =id;
        this.messageCount = messageCount;
    }

    public void run() {
        Socket socket = null;
        try {
            for(int i = 0; i < messageCount; i++) {
                socket = new Socket("localhost", 5001);
                PrintWriter out =
                        new PrintWriter(socket.getOutputStream(), true);
                out.println("hello this is a new message from a examples.client that is kinda like a normal message and I think this should be long enough soon. id=" + clientId + " count=" + messageCount);

                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
