package examples.client;

import sun.util.cldr.CLDRLocaleDataMetaInfo;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;

/**
 * Created by dlbremil on 11/29/17.
 */
public class FileLoggingClient implements Client {
    private final File file = new File("log-test.txt");
    int messageCount;
    int clientId;

    public FileLoggingClient(int id, int messageCount) {
        clientId =id;
        this.messageCount = messageCount;
    }

    public void run() {
        try {
            for (int i = 0; i < messageCount; i++) {
            FileChannel in = FileChannel.open(file.toPath(), StandardOpenOption.APPEND);
            try {
                in.lock();
                        String message ="hello this is a new message from a examples.client that is kinda like a normal message and I think this should be long enough soon. id=" + clientId + " count=" + messageCount;
                        in.write(ByteBuffer.wrap(message.getBytes("UTF-8")));


                } finally {
                    in.close();

                }
            }
        } catch (Exception e) {
            System.err.println("Error while writing to file" + e);
        }
    }
}
