package examples.client;

/**
 * Created by dlbremil on 11/29/17.
 */
public class DoNothingClient implements Client {
    private int messageCount = 20;
    int clientId;

    public DoNothingClient(int id, int messageCount) {
        clientId =id;
        this.messageCount = messageCount;
    }

    public void run() {
       // do nothing and die
    }
}
