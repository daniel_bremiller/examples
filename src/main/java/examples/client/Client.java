package examples.client;

/**
 * Created by dlbremil on 11/29/17.
 */
public interface Client extends Runnable {

    public void run();
}
