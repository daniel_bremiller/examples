package examples.client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ClientSimulator {
    private static int numClients;
    private static int concurrentClients;
    private final ClientType clientType;

    ClientFactory factory = new ClientFactory();

    public ClientSimulator(int numClients, int concurrentClients, ClientType type ) {
        this.numClients = numClients;
        this.concurrentClients = concurrentClients;
        this.clientType = type;
    }

    public void run() {
        try {
            ExecutorService executor = Executors.newFixedThreadPool(concurrentClients);

            long l = System.currentTimeMillis();
            for (int i = 0; i < numClients; i++) {
                executor.submit(factory.getClient(clientType));
            }
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.MINUTES);
            System.out.println("Clients took time=" + (System.currentTimeMillis() - l));
            System.out.println("average time=" + (System.currentTimeMillis() - l) / numClients);
        }catch (Exception e) {
            System.err.println("Error: " + e);
        }
    }
}
