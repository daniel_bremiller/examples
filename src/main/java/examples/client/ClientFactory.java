package examples.client;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ClientFactory {

    int messages = 20000;
    AtomicInteger counter = new AtomicInteger();

    public Client getClient(ClientType type) {
        switch (type) {
            case Socket:
                return new SocketLoggingClient(counter.getAndIncrement(), messages);
            case Console:
                return new ConsoleLoggingClient(counter.getAndIncrement(), messages);
            case File:
                return new FileLoggingClient(counter.getAndIncrement(), messages);
            case DoNothing:
                return new DoNothingClient(counter.getAndIncrement(), messages);
            default:
                throw new RuntimeException("No Client for type");
        }

    }
}
