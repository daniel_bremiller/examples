package examples.client;

/**
 * Created by dlbremil on 11/29/17.
 */
public enum ClientType {
    Socket,
    Console,
    File,
    DoNothing
}
