package examples.client;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ProcessClientSimulator {
    private static int numClients;
    private static int concurrentClients;
    private final ClientType clientType;

    ClientFactory factory = new ClientFactory();

    public ProcessClientSimulator(int numClients, int concurrentClients, ClientType type ) {
        this.numClients = numClients;
        this.concurrentClients = concurrentClients;
        this.clientType = type;
    }

    public void run() {
        try {
            ExecutorService executor = Executors.newFixedThreadPool(concurrentClients);

            long l = System.currentTimeMillis();
            for (int i = 0; i < numClients; i++) {
                executor.submit(new ProcessStarter(clientType));
            }
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.MINUTES);
            System.out.println("Clients took time=" + (System.currentTimeMillis() - l));
            System.out.println("average time=" + (System.currentTimeMillis() - l) / numClients);
        }catch (Exception e) {
            System.err.println("Error: " + e);
        }
    }

    public class ProcessStarter implements Runnable {
        private final ClientType clientType;

        public ProcessStarter(ClientType clientType) {
            this.clientType = clientType;
        }
        @Override
        public void run() {
            try {
                Process process = new ProcessBuilder()
                        .command(new String("java -cp target/examples-1.0-SNAPSHOT.jar examples.client.SingleClientSimulator " +clientType.toString()).split(" "))
                        .start();

                process.waitFor();
            } catch (Exception e) {
                System.err.println("Error: " + e);
            }
        }
    }

    }
