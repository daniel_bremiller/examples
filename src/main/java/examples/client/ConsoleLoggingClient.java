package examples.client;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ConsoleLoggingClient implements Client {
    private int messageCount = 20;
    int clientId;

    public ConsoleLoggingClient(int id, int messageCount) {
        clientId =id;
        this.messageCount = messageCount;
    }

    public void run() {
        for (int i = 0; i < messageCount; i++) {
            System.out.println("hello this is a new message from a examples.client that is kinda like a normal message and I think this should be long enough soon. id=" + clientId + " count=" + messageCount);
        }
    }
}
