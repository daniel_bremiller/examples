package examples.client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by dlbremil on 11/29/17.
 */
public class SingleClientSimulator {
    private static int numClients;
    private static int concurrentClients;
    private final ClientType clientType;

    ClientFactory factory = new ClientFactory();

    public SingleClientSimulator(ClientType type ) {
        this.clientType = type;
    }

    public void run() {
            factory.getClient(clientType).run();
    }

    public static void main(String[] args) {
        if(args.length < 1) {
            throw new RuntimeException("Need the client Type");
        }
        new SingleClientSimulator(ClientType.valueOf(args[0])).run();
    }
}
