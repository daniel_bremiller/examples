package examples;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by dlbremil on 11/28/17.
 */
public class ThreadedLoggingServer extends Thread{

    static final int PORT = 5001;
    private static int clientsRecieved = 0;
    private static int concurrentClients = 4;
    static final AtomicLong messageCount = new AtomicLong();
    private final int clientToRecieve;
    private static final LinkedBlockingQueue<String> logQueue = new LinkedBlockingQueue<>();

    public ThreadedLoggingServer(int clientsToRecieve) {
        this.clientToRecieve = clientsToRecieve-1;
    }

    public void run()  {
        FileLoggingThread fileLoggingThread = new FileLoggingThread(logQueue);
        fileLoggingThread.start();


        ServerSocket serverSocket = null;
        Socket socket = null;

        try {
            serverSocket = new ServerSocket(PORT);
        } catch (Exception e) {
            throw new RuntimeException("port already in use");

        }
        ExecutorService executor = Executors.newFixedThreadPool(concurrentClients);

//        List<Future> results = new ArrayList();
//        float lastMessageCount = 0;
//        float temp = 0;
//        long lastTime = System.nanoTime();
//        long startTime = System.nanoTime();
//        long tempTime = 0;
//        int lastClientRecieved = clientsRecieved;
//        int tempClientRecieved = clientsRecieved;

        while (clientsRecieved <= clientToRecieve) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            // new thread for a examples.client
            try {
                executor.submit(new LoggingThread(socket.getInputStream(), messageCount, logQueue));
            } catch (Exception e) {
                System.err.println("Problem getting socket input stream");
            }

//            clientsRecieved++;
//            temp = messageCount.get();
//            tempTime = System.nanoTime();
//            tempClientRecieved = clientsRecieved;
//            System.out.println("clients recieved=" + clientsRecieved);
////            System.out.println("examples.client messages=" + (tempClientRecieved - lastClientRecieved) / ((tempTime - lastTime)/ 1000000000.0));
//            //System.out.println("examples.client throughput=" + (temp - lastMessageCount) / ((tempTime - lastTime)/ 1000000000.0));
//            lastMessageCount = temp;
//            lastTime = tempTime;
//            lastClientRecieved = tempClientRecieved;
        }

        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            System.err.println(e);
        }
//        temp = messageCount.get();
//        tempTime = System.nanoTime();
//        long totalTime = ((tempTime - startTime)/ 1000000000);
//
//        System.out.println("total time=" + totalTime);
//        System.out.println("clients recieved=" + clientsRecieved);
//        System.out.println("examples.client messages=" + temp);
//        System.out.println("message throughput=" + (temp - lastMessageCount) / ((tempTime - lastTime) / 1000000000.0));
//        System.out.println("total examples.client throughput=" + (clientsRecieved) / totalTime);
//        System.out.println("total message throughput=" + (temp) / totalTime);
    }
}
