package examples.Scenario;

/**
 * Created by dlbremil on 11/29/17.
 */
public abstract class ScenarioBase {
    int numclients = 10000;
    int numMessages = 20;

    public ScenarioBase() {

    }

    public void start() {
        long start = System.nanoTime();
        run();
        double totalTime = (System.nanoTime() - start) / 1000000000.0;
        System.out.println("total time=" + totalTime);
        System.out.println("total examples.client throughput=" + (numclients) / totalTime);
        System.out.println("total message throughput=" + (numMessages * numclients) / totalTime);
    }

    abstract protected void run ();
}
