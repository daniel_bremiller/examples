package examples.Scenario;

import examples.ThreadedLoggingServer;
import examples.client.ClientSimulator;
import examples.client.ClientType;
import examples.client.ProcessClientSimulator;

import java.io.File;
import java.io.IOException;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ProcessNetworkedLoggingScenario extends ScenarioBase{

    public static  void main(String args[]) throws IOException {
        new File("logging-test.txt").delete();
       new ProcessNetworkedLoggingScenario().start();
    }

    protected void run() {
        numclients = 50;
        numMessages = 50;
        ThreadedLoggingServer server = new ThreadedLoggingServer(numclients);
        server.start();

        new ProcessClientSimulator(numclients, numMessages, ClientType.Socket).run();
    }
}
