package examples.Scenario;

import examples.client.ClientSimulator;
import examples.client.ClientType;
import examples.client.ProcessClientSimulator;

import java.io.File;
import java.io.IOException;

/**
 * Created by dlbremil on 11/29/17.
 */
public class FileLoggingScenario extends ScenarioBase {

    public static  void main(String args[]) throws IOException {
        File test = new File("log-test.txt");
        test.delete();
        test.createNewFile();
       new FileLoggingScenario().start();
    }

    protected void run() {
        numclients = 50;
        numMessages = 50;
        new ProcessClientSimulator(numclients, numMessages, ClientType.File).run();
    }
}
