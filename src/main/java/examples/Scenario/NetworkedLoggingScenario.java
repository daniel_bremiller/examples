package examples.Scenario;

import examples.ThreadedLoggingServer;
import examples.client.ClientSimulator;
import examples.client.ClientType;

import java.io.IOException;

/**
 * Created by dlbremil on 11/29/17.
 */
public class NetworkedLoggingScenario extends ScenarioBase{

    public static  void main(String args[]) throws IOException {
       new NetworkedLoggingScenario().start();
    }

    protected void run() {
        ThreadedLoggingServer server = new ThreadedLoggingServer(numclients);
        server.start();

        new ClientSimulator(numclients, numMessages, ClientType.Socket).run();
    }
}
