package examples.Scenario;

import examples.client.ClientType;
import examples.client.ProcessClientSimulator;

import java.io.IOException;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ProcessDoNothingScenario extends ScenarioBase {

    public static  void main(String args[]) throws IOException {
       new ProcessDoNothingScenario().start();
    }

    protected void run() {
        numclients = 1000;
        numMessages = 4;
        new ProcessClientSimulator(numclients, numMessages, ClientType.DoNothing).run();
    }
}
