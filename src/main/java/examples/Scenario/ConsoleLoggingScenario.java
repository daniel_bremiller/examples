package examples.Scenario;

import examples.ThreadedLoggingServer;
import examples.client.ClientSimulator;
import examples.client.ClientType;

import java.io.IOException;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ConsoleLoggingScenario extends ScenarioBase {

    public static  void main(String args[]) throws IOException {
       new ConsoleLoggingScenario().start();
    }

    protected void run() {
        int numclients = 10000;
        int numMessages = 20;
        new ClientSimulator(numclients, numMessages, ClientType.Console).run();
    }
}
