package examples.Scenario;

import examples.client.ClientSimulator;
import examples.client.ClientType;
import examples.client.ProcessClientSimulator;

import java.io.IOException;

/**
 * Created by dlbremil on 11/29/17.
 */
public class ProcessConsoleLoggingScenario extends ScenarioBase {

    public static  void main(String args[]) throws IOException {
       new ProcessConsoleLoggingScenario().start();
    }

    protected void run() {
        numclients = 1000;
        numMessages = 20;
        new ProcessClientSimulator(numclients, numMessages, ClientType.Console).run();
    }
}
