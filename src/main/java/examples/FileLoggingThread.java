package examples;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by dlbremil on 11/29/17.
 */
public class FileLoggingThread extends Thread {

    private final LinkedBlockingQueue<String> logQueue;
    BufferedWriter writer;
    public FileLoggingThread(LinkedBlockingQueue<String> logQueue)  {
        try {
        File f = new File("logging-test.txt");
        writer = new BufferedWriter(new FileWriter(f));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.logQueue = logQueue;
    }

    @Override
    public void run() {
        try {
            try {
                List<String> msgs = new ArrayList<>();
                while (true) {
                    try {
                        msgs.clear();
                        logQueue.drainTo(msgs);
                        for (String message : msgs) {
                            writer.append(message);
                            writer.append(System.lineSeparator());
                        }
                        Thread.sleep(100);
                    } catch (Exception e) {
                        System.err.println("Error: " + e);
                    }
                }
            } finally {
                writer.close();
            }
        } catch (Exception e) {
            System.err.println("fu" + e);
        }

    }
}
