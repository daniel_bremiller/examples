package examples;

import java.io.*;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by dlbremil on 11/28/17.
 */
public class LoggingThread implements Callable<AtomicLong> {

    private final Queue logQueue;
    protected InputStream inputStream;
    private AtomicLong logCount;


    public LoggingThread(InputStream input, AtomicLong logCount, Queue logQueue) {
        this.inputStream = input;
        this.logCount = logCount;
        this.logQueue = logQueue;
    }

    public AtomicLong call() throws Exception {
        InputStream inp = null;
        BufferedReader brinp = null;
        DataOutputStream out = null;
        try {
            inp =inputStream;
            brinp = new BufferedReader(new InputStreamReader(inp));
        } catch (Exception e) {
            return logCount;
        }
        String line;

        try {
            while (true) {
                try {
                    line = brinp.readLine();
                    if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                        inputStream.close();
                        return logCount;
                    } else {
                        logCount.incrementAndGet();
                        logQueue.add(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return logCount;
                }
            }
        }catch (Exception e) {
            System.err.println("Error:" + e);
        }
        return logCount;
    }
}
